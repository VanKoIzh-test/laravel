-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Ноя 11 2020 г., 16:45
-- Версия сервера: 5.7.21-20-beget-5.7.21-20-1-log
-- Версия PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `l91287uv_admin`
--

-- --------------------------------------------------------

--
-- Структура таблицы `failed_jobs`
--
-- Создание: Ноя 10 2020 г., 20:40
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--
-- Создание: Ноя 10 2020 г., 20:40
-- Последнее обновление: Ноя 10 2020 г., 20:40
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_11_09_160532_create_records_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--
-- Создание: Ноя 10 2020 г., 20:40
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `records`
--
-- Создание: Ноя 10 2020 г., 20:40
-- Последнее обновление: Ноя 10 2020 г., 20:43
--

DROP TABLE IF EXISTS `records`;
CREATE TABLE `records` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `records`
--

INSERT INTO `records` (`id`, `title`, `description`) VALUES
(1, 'Sed enim vel temporibus.', 'Да к чему ж ты их сам продай, когда уверен, что выиграешь втрое. — Я тебя ни за какие деньги, ниже\' имения, с улучшениями и без всякого дальнейшего размышления, но — неожиданно удачно. Казенные.'),
(2, 'Placeat ut ut necessitatibus illo dolore ex.', 'Святители, какие страсти! Да не найдешь слов с вами! и поверьте, не было ли рассуждение о бильярдной игре — и стегнул по всем по трем уже не сомневался, что старуха хватила далеко и что те.'),
(3, 'Esse porro sapiente quod cupiditate earum.', 'Ну вот уж и мне рюмку! — сказал — Чичиков, впрочем, отроду не видел ни каурой кобылы, — ни вот на столько не солгал, — — коли высечь, то и затрудняет, что они не твои же крепостные, или грабил бы ты.'),
(4, 'Commodi ut aut aut error deleniti.', 'Селифан ожидал, казалось, мановения, чтобы подкатить под крыльцо, но — за них? — Эх, ты! А и седым волосом еще подернуло! скрягу Плюшкина не знаешь, — того, что отыграл бы, вот как честный.'),
(5, 'Perspiciatis accusantium suscipit aut qui qui aperiam.', 'Насыщенные богатым летом, и без всякого дальнейшего размышления, но — зато уж если сядут где, то сядут надежно и крепко, так что слушающие наконец все отходят, произнесши: «Ну, брат, ты, кажется.'),
(6, 'Laboriosam numquam quae accusantium odio at fugit.', 'Чичиков. — Право, я напрасно время трачу, мне нужно спешить. — Посидите одну минуточку, я вам скажу тоже мое последнее слово: пятьдесят — рублей! Право, убыток себе, дешевле нигде не видно! — После.'),
(7, 'Pariatur exercitationem tempora ut corrupti.', 'Я уж знала это: там все хорошая работа. Третьего года сестра моя — привезла оттуда теплые сапожки для детей: такой прочный товар, до — последней косточки. «Да, — подумал Чичиков и опять прилететь с.'),
(8, 'Eaque quas sit rem consequuntur reprehenderit et.', 'Так ты не держи меня! — Ну вот то-то же, нужно будет завтра похлопотать, чтобы в эту комнату не войдет; нет, это не — охотник играть. — Нет, врешь, ты этого не случится, то все-таки что-нибудь да.'),
(9, 'Ducimus nihil nesciunt nobis ut.', 'Посередине столовой стояли деревянные козлы, и два ружья — одно в триста, а другое в восемьсот рублей. Зять, осмотревши, покачал только головою. Потом были показаны турецкие кинжалы, на одном из.'),
(10, 'Unde accusantium pariatur quas molestias alias.', 'Кувшинников! Мы с Кувшинниковым каждый день завтракали в его бричку. — Говоря — это, Ноздрев показал пустые стойла, где были прежде тоже хорошие лошади. В этой конурке он приладил к стене узенькую.'),
(11, 'Repudiandae inventore non odio fuga deleniti.', 'Зачем же? довольно, если пойдут в пятидесяти. — Нет, нельзя, есть дело. — Ну вот еще, а я-то в чем не бывало, и он, как говорится, в самую силу речи, откуда взялась рысь и дар слова: — Если бы ты в.'),
(12, 'Dolor earum reprehenderit deserunt et nisi.', 'Чичиков не без слабостей, но зато губернатор какой — превосходный человек! — Кто такой? — сказала в это время к окну индейский петух — окно же было очень метко, потому что был чист на своей совести.'),
(13, 'Atque aut provident autem iste.', 'Но знаете ли, — прибавил Манилов, — уж она, бывало, все спрашивает меня: «Да — что курить трубку гораздо здоровее, нежели нюхать табак. В нашем — полку был поручик, прекраснейший и образованнейший.'),
(14, 'Fugiat quia alias nesciunt blanditiis vel.', 'Собакевич очень хладнокровно, — продаст, обманет, — еще вице-губернатор — это Гога и Магога! «Нет, он с весьма значительным видом, что он всякий раз, когда смеялся, был от него без памяти. Он очень.'),
(15, 'Qui eum dignissimos necessitatibus.', 'Не успел Чичиков осмотреться, как уже говорят тебе «ты». Дружбу заведут, кажется, навек: но всегда или на угол печки, или на Кавказ. Нет, эти господа никогда не носил таких косынок. Размотавши.'),
(16, 'Ut ad nisi ea.', 'Живых-то я уступила, вот и прошлый год был такой неурожай, что — гнусно рассказывать, и во все углы комнаты. Погасив свечу, он накрылся ситцевым одеялом и, свернувшись под ним до земли. «Теперь дело.'),
(17, 'Quisquam dolorem consequatur doloremque ut.', 'Собакевич, — Павел — Иванович оставляет нас! — Потому что мы надоели Павлу Ивановичу, — отвечала девчонка. — Куда ж еще вы их хотели пристроить? Да, впрочем, ведь кости и могилы — — говорил Чичиков.'),
(18, 'Eum omnis dolorum ut in ut accusantium.', 'Скоро, однако ж, родственник не преминул усомниться. «Я тебе, Чичиков, — по пятисот рублей. Ведь вот какой народ! Это не те фрикасе, — что двуличный человек! — Кто стучит? чего расходились?.'),
(19, 'Esse nulla laboriosam incidunt numquam quia.', 'Слова хозяйки были прерваны среди излияний своих внезапным и совсем неожиданным образом. Все, не исключая и самого кучера, опомнились и очнулись только тогда, когда на них утверждены и разве кое-где.'),
(20, 'Et aliquid harum officiis aliquid.', 'Чичиков почувствовал в себе столько растительной силы, что бакенбарды скоро вырастали вновь, еще даже лучше прежних. И что по — сту рублей каждую, и очень бы могло составить, так сказать, видно во.'),
(21, 'Magni officia corporis sunt vel omnis.', 'Выдумали диету, лечить голодом! Что у них у — тебя побери, продавай, проклятая!» Когда Ноздрев это говорил, Порфирий принес бутылку. Но Чичиков сказал просто, что подобное предприятие очень трудно.'),
(22, 'Ut quaerat in quos.', 'Ноздрев, указывая пальцем на своего человека, который держал в одной — руке ножик, а в канцелярии, положим, существует правитель канцелярии. Прошу смотреть на него, когда он рассматривал общество, и.'),
(23, 'Ex amet dolores enim.', 'Канари с толстыми ляжками и нескончаемыми усами, Бобелину и дрозда в клетке. «Эк какую баню задал! смотри ты какой!» Тут много было посулено Ноздреву всяких нелегких и сильных желаний; попались даже.'),
(24, 'Architecto eum dolorem sit.', 'Это было у места, потому что теперь ты упишешь полбараньего бока с кашей, закусивши ватрушкою в тарелку, а тогда бы у тебя были собаки. Потом пошли осматривать водяную мельницу, где недоставало.'),
(25, 'Reiciendis autem sit perspiciatis placeat.', 'Вот посмотри-ка, Чичиков, посмотри, какие уши, на-ка — пощупай рукою. — Да что же ты можешь, пересесть вот в его губернию въезжаешь, как в рай, дороги везде бархатные, и что при этом случае очень.'),
(26, 'Quia totam quos officia aut debitis explicabo.', 'Ведь я знаю, что ты теперь не отстанешь, но — за десять тысяч не отдам, наперед говорю. Эй, Порфирий! — закричал опять Ноздрев. — Ну да уж больше в городе губернатор, кто председатель палаты, кто.'),
(27, 'Quo ut repellendus voluptates quam sunt modi.', 'Когда установившиеся пары танцующих притиснули всех к стене, он, заложивши руки назад, глядел на разговаривающих и, как только рессорные. И не просадил бы. Не загни я после пароле на проклятой.'),
(28, 'Nesciunt enim vitae asperiores.', 'Ноздрев повел их в погребе целую зиму; а мертвые души нужны ему для приобретения весу «в обществе, что он наконец тем, что выпустил опять дым, но только уже не по своей — тяжелой натуре, не так.'),
(29, 'Laborum ab eaque facere fugit neque.', 'А может, в хозяйстве-то как-нибудь под случай понадобятся… — — возразила старуха, да и времени берет немного». Хозяйка вышла с тем чтобы накласть его и на свет божий взглянуть! Пропал бы, как.'),
(30, 'Non doloribus excepturi tenetur.', 'Ей-богу, продала. — Ну врешь! врешь! — Однако ж не отойдешь, почувствуешь скуку смертельную. От него не дал, — заметил белокурый. — В театре одна актриса так, каналья, пела, как канарейка!.');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--
-- Создание: Ноя 10 2020 г., 20:40
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `records`
--
ALTER TABLE `records`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `records_title_unique` (`title`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `records`
--
ALTER TABLE `records`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
