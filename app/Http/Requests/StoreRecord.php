<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRecord extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|max:255|unique:records,title,$this->id",
            "description" => "required|min:5"
        ];
    }

    public function messages()
    {
        return [
            'title.unique' => 'Такой заголовок уже существует',
        ];
    }


}
