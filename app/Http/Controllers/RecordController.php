<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRecord;
use App\Models\Record;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RecordController extends Controller
{
    public function index(Request $request)
    {
        $request->session()->put('page', $request->page);
        $records = Record::query()->orderBy('id', 'desc')
                                  ->paginate(5);

        return view('welcome', compact('records'));
    }

    public function addForm(Request $request)
    {
        return view('addForm');
    }

    public function add(StoreRecord $request)
    {
        Record::create([
            'title' => $request->input('title'),
            'description' => $request->input('description')
        ]);

        return redirect()->route('recordsList');
    }

    public function editForm(Request $request)
    {
        $record = Record::findOrFail($request->id);

        return view('editForm', compact('record'));
    }

    public function edit(StoreRecord $request)
    {
        Record::findOrFail($request->id)->update([
            'title' => $request->input('title'),
            'description' => $request->input('description')
        ]);

        return redirect()->route('recordsList', ['page' => Session::get('page')]);
    }

    public function delete(Request $request)
    {
        Record::findOrFail($request->id)->delete();

        return redirect()->route('recordsList', ['page' => Session::get('page')]);
    }

}
