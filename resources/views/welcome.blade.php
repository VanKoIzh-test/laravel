@extends('layouts.app')

@section('content')
    <a href="{{route('recordAdd')}}" class="btn btn-success mb-3">Добавить пластинку</a>
    @foreach($records as $record)
        <div class="card mb-3">
            <div class="card-body">
                <h5 class="card-title">{{$record->title}}</h5>
                <p class="card-text">{{$record->description}}</p>
                <a href="{{route('recordEdit',$record->id)}}" class="btn btn-primary">редактировать</a>
                <a href="{{route('recordDelete',$record->id)}}" class="btn btn-danger">удалить</a>
            </div>
        </div>
    @endforeach
    {{ $records->links() }}
@endsection
