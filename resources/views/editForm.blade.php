@extends('layouts.app')

@section('content')
    <a href="{{route('recordsList',['page'=>Session::get('page')])}}" class="btn btn-primary mb-3">Назад к списку</a>
    <form action="{{route('recordEdit',$record->id)}}" method="post">
        @csrf
        <div class="form-group">
            <label>Заголовок</label>
            <input name="title" type="text" value="{{old('title',$record->title)}}" class="form-control">
            @error('title')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Описание</label>
            <textarea name="description" class="form-control"
                      rows="5">{{old('description',$record->description)}}</textarea>
            @error('description')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
@endsection
