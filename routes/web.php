<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RecordController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [RecordController::class,'index'])->name('recordsList');
Route::get('/add',[RecordController::class,'addForm'])
    ->name('recordAdd');
Route::post('/add',[RecordController::class,'add']);
Route::get('/edit/{id}',[RecordController::class,'editForm'])
    ->name('recordEdit');
Route::post('/edit/{id}',[RecordController::class,'edit']);
Route::get('/delete/{id}', [RecordController::class,'delete'])
    ->name('recordDelete');
